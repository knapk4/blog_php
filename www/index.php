<?php
    require("connection.php");
?>
<?php
    	
	if (isset($_POST['submitconnexion']))	
    {
    	$name_connect = htmlspecialchars($_POST['nameconnect']);
    	$motdepasse = md5($_POST['pass']);
    	if(!empty($name_connect) AND !empty($motdepasse)) 
    		{
    			$requser = $bdd->prepare("SELECT * FROM users WHERE username = ? AND password = ?");
      			$requser->execute(array($name_connect,$motdepasse));
      			$userexist = $requser->rowCount();
	    			if($userexist == 1) {
	         			$userinfo = $requser->fetch();
	         			$_SESSION['id'] = $userinfo['id'];
	       				$_SESSION['username'] = $userinfo['username'];
	       				header("Location:profil.php?id=".$_SESSION['id']);
	      				} 
	      				else 
	      				{
	         				echo "Mauvais nom ou password !";
	   					} 
	   				}	else 
	   					{
	      					echo "Tous les champs doivent être complétés !";
	   					}
					
   	}
				   		?>
				    		
				        
				        
				       	<?php
    					?>

<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="UTF-8">
		<title>Blog !</title>
		<link rel="stylesheet" type="text/css" href="dist/css/main.min.css">
	</head>

	<body>
		<header>

			<h1>Mon Blog</h1>

		</header>

		<nav>
			<ul>
				<li>Home</li>
				<li>Page</li>
				<li>Billet de blog</li>			
			</ul>
		


			<form action="profil.php" method="POST">
				<p>
	   				<input type="text" name="nameconnect" placeholder="username" />
	   				<input type="password" name="pass" placeholder="password" />
	   				<input type="submit" name="submitconnexion" value="Valider" />
				</p>			
			</form>
		</nav>

		<section>

			<div class="buton">
				<button>Filtrer par auteur</button>
			</div>

			<?php
				$articles = $bdd->query('SELECT articles.*, users.username FROM articles INNER JOIN users ON articles.author=users.id');

				while ($row = $articles->fetch()) {
			?>

					<div class="article">				
							
						<img src ="<?php echo $row["image"];?>">
						<h2><a href="Fiche.php?id=<?php echo $row ['id'];?>">
						<?php echo $row ['title'];?>					
						</a></h2>
						<h3> "<?php echo $row ['extract'];?>"</h3>
						<p> <?php echo $row ['username'];?></p>

					</div>
				
			<?php
			}
			?>

		</section>
		
	</body>
</html>



