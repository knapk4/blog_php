# PHP BLOG FROM SCRATCH

Le but de cet exercice est de créer un blog en PHP sans aucun framework PHP.

## Consignes

Créer le menu suivant :

Home - Page - Billet de blog - Login

### Home
Cette page liste tous les articles classés du plus récent au plus ancien
Pour chaque article doit être affiché le titre, l'extrait, l'image, l'autre et la date de publication.

### Page Billet blog
Cette page affiche un article complet (titre, contenu, l'image, l'autre et la date de publication).

### Login
Cette page doit permettre à un utilisateur de se connecter. 
Une fois connecter, l'utilisateur doit pouvoir :  
* créer un nouvel artcle
* modifier ses propres articles
* supprimer ses propres articles.

Le mot de passe est le même pour tous les utilisateurs. Le hash utilise MD5.

### En plus
Rajouter la possibilité de filter la liste des articles par auteur (sur la page Home)

## Données

Vous devez utiliser la base de données disponible dans le dossier data/ de ce répertoire gitlab.

## Install

Run "npm install"

## Developpement

Run "npw gulp"